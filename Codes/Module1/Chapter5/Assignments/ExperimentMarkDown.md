#Virtual Lab

![Logo](https://www.iitg.ac.in/cseweb/vlab/img/logo1.png)
## Aim
1. To Understand and Implement Data Defining Language (DDL) Statements.
## Objectives :
+Creating a table, with or without constraints.<br>
+Understanding Data types.<br>
+Altering the structure of the table like adding attributes at later stage, modifying size of attributes or adding constraints to attributes.<br>
+Removing the table created, i.e Drop table in SQL.<br>

<hr>

## Theory

1. CREATE TABLE

The CREATE TABLE statement is used to create a table in SQL. A table comprises of rows and columns. So while creating tables we need to provide all the information to SQL about the names of the columns, type of data to be stored in columns, size of the data, constraints if any, etc.

1. Basic Syntax

create table <table_name>
( <column_name> <data_type> (<size>) [primary key] [not null],

<column_name> <data_type> (<size>) [primary key] [not null],

<column_name> <data_type> (<size>) [primary key] [not null],

.

.

<column_name> <data_type> (<size>) [primary key] [not null],

[primary key (<column_name>[,<column_name>,<column_name>..,<column_name>])]

);
<br><br>
2. Description<br>
table_name                         : name of the table.

column1                               : name of the first column.

data_type                            : Type of data we want to store in the particular column.

For example number for numeric data.

size                                         : Size of the data we can store in a particular column.

For example if for a column we specify the data_type as number and size as 10 then this column can store number of maximum 10 digits.
<br><br>
3. Example<br>
If you want to create a table called Employee which has all the attributes like Empid, Ename, Address, Salary.

Then, issue the following command in the editor

Create table Employee (Empid varchar(6) , Ename varchar(15) , Address varchar(25) , Salary number(5));

 
<hr>

## Procedure

1. Procedure
   1. When you visit the first experiment DDL statements, you will observe various tabs like Theory, Procedure, Pretest, Simulator, posttest and References.
   2. You should read the Theory of the concept and then go to the Simulator.
   3. On clicking the Simulator tab, the Query editor opens, wherein you can write various queries studied in the document.
   4. Write Query in the Query Editor and click on Execute Query button.
   5. if you are existing user and want to save/restore your data, use Credentials.
   6. Credentials are useful when you want to save your data, For e.g if you have created a table and now wish to insert data at a later stage, just save your credentials in form of a file and restore the same, you visit next time.

